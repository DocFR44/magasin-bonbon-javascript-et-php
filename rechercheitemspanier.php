<?php
header("Content-type: application/json; charset=utf-8");
header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header("Pragma: no-cache");

$msgErreur = null;

	require("includes/param_bd.inc");
	require("includes/session.php");
	try 
	{
		$connBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$connBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	} 
	catch (PDOException $e) 
	{
		exit( "Erreur lors de la connexion � la BD :<br />\n" .  $e->getMessage() );
	}
		if(!empty($_SESSION['panier']))
		{
			$infoItem = "[\n";
			foreach(array_keys($_SESSION['panier']) as $item)
			{
				try
				{
					$req = $connBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
					$req->execute(array('item'=>$item));
					
					$info = $req->fetch();
					$no = $info['no'];
					$nom = $info['nom'];
					$image = $info['imageMini'];
					$prix = $info['prix'];
					$quantite = $_SESSION['panier'][$item];
			

				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arr�te tout
						die('Erreur : '.$e->getMessage());
				}		
						
				$infoItem .= "\t{\n";
				$infoItem .= "\t\t\"no\": \"$no\",\n";
				$infoItem .= "\t\t\"nom\": \"$nom\",\n";
				$infoItem .= "\t\t\"image\": \"$image\",\n";
				$infoItem .= "\t\t\"prix\": \"$prix\",\n";
				$infoItem .= "\t\t\"quantite\": \"$quantite\"\n";
				$infoItem .= "\t},\n";
				$req->closeCursor();
			}
			$infoItem .= "]";
			
			$posDerniereVirgule = strrpos($infoItem,",");
			
			$infoItem = substr_replace($infoItem,"",$posDerniereVirgule,1);

			echo $infoItem;

		}

	$connBD = null;

?>