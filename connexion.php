<!-- CETTE PAGE EST SÉCURISÉE SSL (https)!-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | connexion</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		?>
		<!-- Menu principal -->
					<?php
			include 'includes/menu.php';
		?>

		<!-- Contenue -->
		<section id="contenu">
		<h2>connexion</h2><div class="souligne"></div>
		<div id="connexion">
		<?php

			if(isset($_GET['commande']))
			{
				echo "Vous devez vous inscrire ou vous connecter avant de payer";
			}
		?>

			<form action="includes/authentification.php" method="post">
				<p>
					<label for="login">login: </label>
					<input type="text" name="login" id="login" class="vertpale"
					<?php
					if(isset($_COOKIE['login']))
					{
						echo 'value="'.$_COOKIE['login'].'"';
					}
					?>
					/>
				</p>
				<p>
					<label for="passe">mot de passe: </label>
					<input type="password" name="passe" id="passe" class="vertpale"/>
				</p>
				<p> 			
				<a href="nousjoindre.php">Vous avez oublié votre mot de passe?</a> 
				</p>
				<fieldset class="souvenir">
					<input type="checkbox"  name="souvenir"  id="souvenir"  value="souvenir"  checked="checked" />
					<label for="souvenir">Se souvenir de moi</label>
				</fieldset>
				<p>
					<input type="button" value="S'inscrire" class="btn" onclick="location.href='inscription.php'"/>
					<input type="submit" value="Se connecter" id ="btnconnecter" class="btn"/>
				</p>

			</form>
			<?php
			if(isset($_GET['erreur']))
			{
				if($_GET['erreur']=="login")
				{
					echo '<p>Erreur login</p>';
				}
				elseif($_GET['erreur']=="motpasse")
				{
					echo '<p>Erreur Mot de passe</p>';
				}
			}
		
				?>
		</div>
		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
</body>
</html>