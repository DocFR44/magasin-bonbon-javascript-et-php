<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | acceuil</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		?>
		<!-- Menu principal -->
					<?php
			include 'includes/menu.php';
		?>

		<!-- Contenue -->
		<section id="contenu">
			<div id="solde">
				<img id ="images_promotions" src="images/images_promotions/img0.png" alt="Magnifique image de jujube en forme de nounours" />
				<div id="point">	
					<ul id="liste">
						<li><img id="img0" src="images/images_promotions/pico2.png"/></li>
						<li><img id="img1" src="images/images_promotions/pico1.png"/></li>
						<li><img id="img2" src="images/images_promotions/pico1.png"/></li>
					</ul>
				</div>
			</div>

			<h2>Bienvenue !</h2><div class="souligne"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur eu quam nec volutpat. Maecenas semper libero in dui rutrum porta. Fusce dictum arcu justo, quis aliquet arcu tempus non. Sed suscipit enim non purus suscipit, id fermentum felis dictum. Maecenas tempus congue metus, gravida hendrerit sem lacinia at. Morbi rutrum condimentum semper. Maecenas eget aliquet diam, vitae volutpat dui. Morbi scelerisque magna enim, quis efficitur risus molestie eu. Duis nisl eros, iaculis a rutrum vitae, viverra sit amet quam. Donec a velit sed est placerat vulputate. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque volutpat sagittis ligula, id interdum metus dignissim a. Integer suscipit justo eu nibh sodales cursus. Sed maximus eros nisi, non feugiat ipsum venenatis sed.</p>

			<p>Ut rhoncus eros mauris. Sed tempus mauris purus, ut aliquet lectus feugiat ut. Fusce malesuada, urna vitae vestibulum dapibus, mi dui interdum urna, ac gravida enim ex id ligula. Ut sit amet felis eu nulla tristique aliquet. Vivamus et odio non augue ornare ultrices. Ut lectus dolor, fringilla semper dolor quis, lobortis fringilla nibh. Integer metus libero, dignissim in consequat sit amet, mollis ut tortor. In dolor dolor, lobortis et sapien eu, laoreet condimentum enim. Morbi lobortis massa quam, vel blandit urna scelerisque vel.</p>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
	  <script type="text/javascript" src="js/visionneuse.js"></script>
</body>
</html>
