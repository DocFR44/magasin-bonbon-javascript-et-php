<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | à propos</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		?>
		<!-- Menu principal -->
					<?php
			include 'includes/menu.php';
		?>
		<!-- Contenue -->
		<section id="contenu">
			<img src="images/aproposimage.jpg" alt="Magnifique image de jujube dans notre magasin" />
			<h2>à propos de nous</h2><div class="souligne"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis tortor vehicula arcu laoreet, nec fermentum libero tristique. Ut enim dolor, volutpat sed erat eget, efficitur porttitor mauris. Vestibulum tincidunt porta bibendum. Nunc laoreet convallis ligula auctor finibus. Vivamus sagittis orci in interdum sollicitudin. Proin ut congue odio, at vulputate lorem. Donec et consectetur libero. Mauris ac tempor neque. Nulla sed elit vel augue venenatis dapibus nec nec eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam ut sapien eget diam laoreet commodo in at tortor. Nulla facilisi. In dapibus varius placerat. Ut commodo luctus mauris. Aenean commodo velit erat, et semper massa eleifend ac. Suspendisse sed tortor sed libero rutrum cursus sit amet sed odio.</p>

			<p>Morbi vel tincidunt leo, sit amet ultrices mi. Praesent a risus a enim fermentum molestie eu id tellus. Integer id nunc id nunc blandit mattis. Donec orci tortor, aliquet eu nisi a, pharetra rhoncus ipsum. Nulla neque ipsum, pellentesque ut tempus at, rutrum in mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In dignissim est non dui tincidunt tristique. Pellentesque varius, magna id condimentum elementum, magna turpis vulputate ante, quis vulputate eros magna fermentum justo. Suspendisse id neque eget arcu interdum posuere a ac ligula.</p>
		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
</body>
</html>
