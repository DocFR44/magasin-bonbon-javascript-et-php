<?php
			$rupture = '';
			include 'header.php';
			include 'param_bd.inc';
				try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				
				try
				{
				$req = $connexionBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
				$req->execute(array('item'=>$_GET['numproduit']));

				$infoItem = $req->fetch();

				$req->closeCursor();
				$connexionBD = null;
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
			if($infoItem['poids'] * 1000 >= (($_SESSION['panier'][$_GET['numproduit']]+1) * 50))
			{
				if(isset($_SESSION['panier'][$_GET['numproduit']]))
				{
					$_SESSION['panier'][$_GET['numproduit']] +=1;
				}
				else
				{
					$_SESSION['panier'][$_GET['numproduit']] = 1;
				}			
			}
			else
			{
				$rupture = '&rupture=vrai';
			}
			
		if (isset($_GET['typebonbon']))
		{
			header('Location:../'.$_GET['pageprecedente'].'.php?typebonbon='.$_GET['typebonbon'].'&numproduit='.$_GET['numproduit'].$rupture);
		}
		else
		{
			header('Location:../'.$_GET['pageprecedente'].'.php?numproduit='.$_GET['numproduit'].$rupture);
		}

	exit;
?>