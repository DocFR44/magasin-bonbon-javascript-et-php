<?php
include 'header.php';
include 'param_bd.inc';
	try
	{
		// On se connecte à MySQL
		$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
		$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}
	catch(Exception $e)
	{
		// En cas d'erreur, on affiche un message et on arrête tout
			die('Erreur : '.$e->getMessage());
	}

if ($_POST['login'] != "")
{
	try
	{
	$req = $connexionBD->prepare('SELECT * FROM clients WHERE clients.login = :item');
	$req->execute(array('item'=>$_POST['login']));	
	$infoItem = $req->fetch();
	$req->closeCursor();
	$connexionBD = null;
	}
	catch(Exception $e)
	{
		// En cas d'erreur, on affiche un message et on arrête tout
			die('Erreur : '.$e->getMessage());
	}
	if(isset($infoItem['login']))
	{
		if (hash('sha256',$_POST['passe']) == $infoItem['motPasse'])
		{
			$_SESSION['login'] = $_POST['login'];
			
			if($_POST['souvenir'] == "souvenir")
			{
				setcookie("login", $_SESSION['login'], time()+86400, '/');
			}
			// ou $_SESSION['secure'] = true;
			header('location:../confirmation.php?confirmer=connecte');
			exit;
		}
		else 
		{
			header('location:../connexion.php?erreur=motpasse');
			exit;
			
		}
	}
	else
	{
		header('location:../connexion.php?erreur=login');
		exit;
	}
}
else
{
	header('location:../connexion.php?erreur=login');
	exit;
}