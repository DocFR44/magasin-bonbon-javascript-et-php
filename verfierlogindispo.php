<?php
header("Content-type: application/json; charset=utf-8");
header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header("Pragma: no-cache");

$msgErreur = null;

if ( ! isset($_GET["login"]) )
	$msgErreur = "Le param�tre \"login\" n'a pas �t� fourni avec la requ�te.";
else
{
	// Param�tres de connexion � la BD.
	require("includes/param_bd.inc");
	// Cr�ation d'une connexion � la BD.
	try {
		$connBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$connBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	} catch (PDOException $e) {
		exit( "Erreur lors de la connexion � la BD :<br />\n" .  $e->getMessage() );
	}

	$rech = $_GET["login"];
	
	try {
		$req = "SELECT * FROM clients WHERE login LIKE :champlogin";
		$prepReq = $connBD -> prepare($req);
		$prepReq -> execute( array( "champlogin" => '%'.$rech.'%') );
		$prepReq -> setFetchMode(PDO::FETCH_OBJ);
	} catch (PDOException $e) {
		exit( "Erreur lors de l'ex�cution de la requ�te SQL :<br />\n" .  $e -> getMessage() . "<br />\nREQU�TE = " . $req );
	}

	// Est-ce que le membre existe ? Si oui, on lit le premier enregistrement
	if ($prepReq->rowCount() == 0)
		$msgErreur = true;
	else
	{
		$msgErreur = false;
	}

		echo "{\n";
		echo "\"message\": \"" . str_replace("\"", "\\\"", $msgErreur) . "\"\n";
		echo "}\n";
	$prepReq -> closeCursor();
	$connBD = null;
}
?>