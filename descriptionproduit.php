<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | description produit</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
			include 'includes/menu.php';
			include 'includes/param_bd.inc';
		?>

		<!-- Contenu -->
			<section id="contenu">
				<?php

				try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}

				try
				{
				$req = $connexionBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
				$req->execute(array('item'=>$_GET['numproduit']));
				
				$infoItem = $req->fetch();

				$req->closeCursor();
				$connexionBD = null;
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
			?>
					<h2><?php echo $infoItem['nom']; ?></h2><div class="souligne"></div>

					<div id="description">
						<img src ="images/produits_grands/<?php echo $infoItem['imageGrande']; ?>" alt=" <?php echo $infoItem['nom']; ?> "/>
						<div id="info">
							<p><?php echo $infoItem['prix']; ?>/50g</p>
							<p><?php echo $infoItem['description'] ?></p>
						</div>

						<!--BOUTON !!! AJOUTER AU PANIER -->
						<a href="includes/ajoutpanier.php?numproduit=<?php echo $_GET['numproduit']; ?>&pageprecedente=descriptionproduit" class="btn"  id="aj<?php echo $_GET['numproduit']; ?>">Ajouter au panier</a>
					</div>
					
				
			<?php
			
			?>
			</section><!-- Fin de la section "contenu" -->
					<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
			<script src="js/produit.js"></script>
</body>
</html>