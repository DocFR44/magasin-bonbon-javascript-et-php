-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 03 Novembre 2016 à 15:24
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp1_fhamel_elmarion`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories_produits`
--

CREATE TABLE `categories_produits` (
  `ID` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories_produits`
--

INSERT INTO `categories_produits` (`ID`, `nom`) VALUES
(1, 'jujubes'),
(2, 'bonbons durs'),
(3, 'réglisses');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `no` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `province` varchar(40) NOT NULL,
  `codePostal` char(7) NOT NULL,
  `login` varchar(20) NOT NULL,
  `motPasse` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`no`, `nom`, `prenom`, `adresse`, `ville`, `province`, `codePostal`, `login`, `motPasse`, `email`) VALUES
(1, 'fasds', 'fdsa', '1234 aSad', 'Quebec', 'Québec', 'G3J 1S6', 'DocFR44', 'baba', 'francis.hamel.2@outaaaa.com'),
(2, 'userasd', 'userasd', 'userasd', 'userabc', 'Qu�bec', 'G3J 1S7', 'user', '123', 'abc2@abc.abc'),
(3, 'franciq', 'hamek', '<nav><h2>1439ruedesimperatrice</h2></nav>', 'quebec', 'Québec', 'G3J 1S7', 'doc', '123', 'francis.hamel.2@outlook.ca'),
(4, 'francis', 'hamel', '<h2>blabla</h2>', 'quebec', 'Québec', 'G3J 1S7', 'docky123', '123', 'localhost@google.ca'),
(5, 'Marion', 'EveLyne', '123,vv faffaa', 'Québec', 'Québec', 'G1X 3W8', 'elmarion', 'qwerty123', 'webmestre@assofxg.com'),
(6, 'gjjgj', 'gjgjgjgj', '12j2j2j2j', 'rrr', 'Québec', 'G1X 3W8', 'gigi', '1eb1fd0bdeaa0829827a6669141b25d9a278b892954665042d8d21ab0d42e22e', 'fff@ggg.com'),
(7, 'Marion', 'Eve-Lyne', '34, rue de n\'importe quoi', 'Québec', 'Québec', 'G1X 3W8', 'qwerty', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'eve@qqchose.com'),
(8, 'adada', 'ddd', 'afagsbnfdgjsdhklfyutrstueysg«', 'dddd', 'Québec', 'G1X 3W8', 'adadadad', 'fc7072580e5db8e1e425464e8eeb769beb6fcb3892e4db8507c2abefb86b3c60', 'eve@qqchoffffse.com');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `no` int(11) NOT NULL,
  `date` date NOT NULL,
  `statut` varchar(50) NOT NULL,
  `typePaiement` varchar(50) NOT NULL,
  `noClient` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`no`, `date`, `statut`, `typePaiement`, `noClient`) VALUES
(1, '2016-09-30', 'En paiement', 'Visa', 1),
(2, '2016-09-30', 'En paiement', 'Visa', 1),
(3, '2016-09-30', 'En paiement', 'Paypal', 1),
(4, '2016-09-30', 'En paiement', 'Visa', 1),
(5, '2016-09-30', 'En paiement', 'Visa', 1),
(6, '2016-09-30', 'En paiement', 'Visa', 1),
(7, '2016-10-02', 'En paiement', 'Visa', 1),
(8, '2016-10-02', 'En paiement', 'Visa', 5),
(9, '2016-10-02', 'En paiement', 'Visa', 5),
(10, '2016-10-02', 'En paiement', 'Visa', 5);

-- --------------------------------------------------------

--
-- Structure de la table `items_commande`
--

CREATE TABLE `items_commande` (
  `noCommande` int(11) NOT NULL,
  `noProduit` int(11) NOT NULL,
  `qte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `items_commande`
--

INSERT INTO `items_commande` (`noCommande`, `noProduit`, `qte`) VALUES
(1, 1, 2),
(1, 3, 1),
(2, 1, 2),
(2, 3, 1),
(3, 12, 2),
(4, 11, 1),
(4, 12, 2),
(4, 15, 1),
(5, 1, 1),
(5, 4, 1),
(5, 12, 1),
(6, 1, 2),
(6, 2, 2),
(6, 11, 2),
(6, 12, 2),
(7, 1, 3),
(7, 2, 1),
(7, 3, 1),
(8, 1, 1),
(8, 2, 1),
(8, 4, 2),
(8, 5, 4),
(9, 1, 1),
(9, 2, 1),
(9, 4, 2),
(9, 5, 4),
(10, 1, 1),
(10, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `no` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `prix` decimal(7,2) NOT NULL,
  `poids` int(11) NOT NULL,
  `dateParution` date NOT NULL,
  `imageMini` varchar(75) NOT NULL,
  `imagePetite` varchar(100) NOT NULL,
  `imageGrande` varchar(50) NOT NULL,
  `categorie` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`no`, `nom`, `description`, `prix`, `poids`, `dateParution`, `imageMini`, `imagePetite`, `imageGrande`, `categorie`) VALUES
(1, 'Anneau bleu', 'Légèrement acidulé, cet anneau est une friandise succulente à la framboise bleue. ', '2.00', 150, '2016-09-16', 'anneau_mini.jpg', 'anneau_petit.jpg', 'anneau_gr.jpg', 1),
(2, 'Cola', 'Vous avez toujours rêvé de mâcher votre cola ? C\'est maintenant possible grâce à ce jujube d\'une excellente qualité !', '3.00', 75, '2016-09-16', 'cola_mini.jpg', 'cola_petit.jpg', 'cola_gr.jpg', 1),
(3, 'Grenouille', 'Si vous aimez les différences de textures cette petite grenouille est votre rêve qui se réalise enfin !', '2.50', 95, '2016-09-16', 'grenouille_mini.jpg', 'grenouille_petit.jpg', 'grenouille_gr.jpg', 1),
(4, 'Oursons', 'Les meilleurs vendeurs vous ne pourrez résister à cette variété de saveurs !', '2.50', 250, '2016-09-16', 'gummybears_mini.jpg', 'gummybears_petit.jpg', 'gummybears_gr.jpg', 1),
(5, 'Vers de terre', 'Deux saveurs sur un même jujubes ! Oui ils ont vraiment l\'air de venir de la terre, mais ils sont si bons !', '2.00', 150, '2016-09-16', 'vers_mini.jpg', 'vers_petit.jpg', 'vers_gr.jpg', 1),
(6, 'Vers de terre acidulés', 'Deux saveurs sur un même jujubes mais acidulé ! Ils sont plus coriaces que leurs cousins, mais ils sont tout aussi délicieux !', '3.00', 60, '2016-09-16', 'verssure_mini.jpg', 'verssure_petit.jpg', 'verssure_gr.jpg', 1),
(7, 'Fruits', 'C\'est un bol de fruits! Ils ont tout pour vous plaire, la forme, la couleur et même le goût ! Mais en bien plus petits et mignons !', '3.50', 125, '2016-09-16', 'fruits_mini.jpg', 'fruits_petit.jpg', 'fruits_gr.jpg', 2),
(8, 'Minis rigolos', 'Ils sont tout petits et ils ont des petits dessins rigolos ! Comment sont imprimés ces minis dessins, c\'est un secret !', '3.00', 65, '2016-09-16', 'dureimprime_mini.jpg', 'dureimprime_petit.jpg', 'dureimprime_gr.jpg', 2),
(9, 'Ronds et emballés', 'Ils sont tous emballés individuellement, comme ça vous pourrez partager ! Idéale pour les cadeaux de fête d\'enfant.', '4.00', 80, '2016-09-16', 'rondemballe_mini.jpg', 'rondemballe_petit.jpg', 'rondemballe_gr.jpg', 2),
(10, 'Ronds et rayés', 'Ceci sont pour les nostalgique de ce monde. Ils goûtent les bonbons d\'autrefois !', '2.50', 75, '2016-09-16', 'rondraye_mini.jpg', 'rondraye_petit.jpg', 'rondraye_gr.jpg', 2),
(11, 'Warheads', 'Attaché votre tuque avec de la broche ! Ces bonbons ont le plus d\'acide permis par l\'industrie ! Ils sont pour les grands fans ! ', '4.00', 150, '2016-09-16', 'warheads_mini.jpg', 'warheads_petit.jpg', 'warheads_gr.jpg', 2),
(12, 'Réglisse aux fraises', 'C\'est la classique ! Quand on pense à de la réglisse c\'est à celle-ci!', '2.50', 150, '2016-09-16', 'fraise_mini.jpg', 'fraise_petit.jpg', 'fraise_gr.jpg', 3),
(13, 'Réglisses aux fraises acidulées', 'Le même bon goût de fraises que la classique mais avec ce petit côté acidulé qui vous fait grimacer.', '3.50', 150, '2016-09-16', 'fraisesure_mini.jpg', 'fraisesure_petit.jpg', 'fraisesure_gr.jpg', 3),
(14, 'Réglisses multicolores', 'Vous pouvez manger chaque couleur individuellement ou tout en même temps cela vous fera, à coup sûre, retrouver votre cœur d\'enfant.', '5.00', 90, '2016-09-16', 'regmultirayee_mini.jpg', 'regmultirayee_petit.jpg', 'regmultirayee_gr.jpg', 3),
(15, 'Réglisse noire', 'C\'est pas pour tout le monde! Quand on l\'aime, on l\'aime ! Ce bon goût aniser, un régale!', '3.50', 75, '2016-09-16', 'regnoire_mini.jpg', 'regnoire_petit.jpg', 'regnoire_gr.jpg', 3),
(16, 'Réglisses aux pommes acidulées', 'Une véritable explosion de pomme verte! C\'est si bon et si acidulé !', '5.00', 95, '2016-09-16', 'regpommesure_mini.jpg', 'regpommesure.jpg', 'regpommesure_gr.jpg', 3),
(17, 'Réglisses acidulées coupées', 'Des réglisses en bouchée ! Pourquoi pas ! ', '5.00', 150, '2016-09-16', 'regsurettecoupee_mini.jpg', 'regsurettecoupee_petit.jpg', 'regsurettecoupee_gr.jpg', 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categories_produits`
--
ALTER TABLE `categories_produits`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`no`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`no`);

--
-- Index pour la table `items_commande`
--
ALTER TABLE `items_commande`
  ADD PRIMARY KEY (`noCommande`,`noProduit`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`no`),
  ADD KEY `categorie` (`categorie`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categories_produits`
--
ALTER TABLE `categories_produits`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `categories_produits` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
