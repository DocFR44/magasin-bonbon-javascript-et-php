<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | produits</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		// Menu principal -->

			include 'includes/menu.php';
			include 'includes/param_bd.inc';
		?>
		<div id="conteneur-menu-produit">
		<!-- Menu Produit -->
			<nav id="menu-produit">
			<form method="get" action="serveur-get.php" id="form-rech">
					<input type="input" name="recherche" id="recherche" placeholder="Recherche..." autocomplete="off" />
				<div id="listeItems"></div>
				<p id=msg-erreur>
				</p>
					<p id="chargement">
						<img src="images/chargement.gif" id="img-chargement-ajax" />
						Chargement en cours ...
					</p>
			</form>
				<ul>
					<li>
					<li>
						<a href="produits.php?typebonbon=jujubes">jujubes</a>	
					</li>
					<li>
						<a href="produits.php?typebonbon=bonbonsdurs">bonbons durs</a>                                             
					</li>
					<li>
						<a href="produits.php?typebonbon=reglisse">réglisses</a>
					</li>
					<li>
						<a href="produits.php">tous les bonbons</a>
					</li>
				</ul>
			</nav>
			<!-- Contenu -->
			<section id="contenu-produit">
			<?php
				try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				
			if(isset($_GET['recherche']))
			{
				try
				{
				$tablebonbons = $connexionBD->prepare('SELECT * FROM produits WHERE nom LIKE :nom');
				$tablebonbons->execute(array('nom'=> '%'.$_GET['recherche'].'%'));
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				?>
				<h2>Résultat de la recherche</h2><div class="souligne"></div>
				<?php
				while($donnees = $tablebonbons->fetch())
					{
					?>
						<div id="bonbons">
							<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><img src="images/produits_petits/<?php echo $donnees['imagePetite']; ?>" alt=" <?php echo $donnees['nom']; ?> "/></a>

							<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><h3><?php echo $donnees['nom']; ?></h3></a>
						</div>
						<div id="prix">
								<p><?php echo $donnees['prix']; ?>/50g</p>
								<!--BOUTON !!! AJOUTER AU PANIER -->
								<a href="includes/ajoutpanier.php?numproduit=<?php echo $donnees['no']; ?>&pageprecedente=produits" class="btn" id="aj<?php echo $donnees['no']; ?>">Ajouter au panier</a>
								
								<?php
									if(isset($_GET['rupture']) AND isset($_GET['numproduit']))
									{
										if($_GET['numproduit'] == $donnees['no'])
										{
											echo '<p><strong>Rupture de stock</strong></p>';
										}
									}
								?>
						</div>		
					<?php
					}
			}
			else
			{
				try
				{
				$tablebonbons = $connexionBD->prepare('SELECT * FROM produits');
				$tablebonbons->execute();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}

				if (isset($_GET['typebonbon']))
				{

					if($_GET['typebonbon'] == "jujubes")
					{

						echo "<h2>jujubes</h2>";
						?>
						<div class="souligne"></div>
						<?php
						$categorie = 1;
						
					}
					elseif($_GET['typebonbon'] == "bonbonsdurs")
					{
						echo "<h2>bonbons durs</h2>";
						?>
						<div class="souligne"></div>
						<?php
						$categorie = 2;
					}
					elseif($_GET['typebonbon'] == "reglisse")
					{
						echo "<h2>réglisse</h2>";
						?>
						<div class="souligne"></div>
						<?php
						$categorie = 3;
					}

					while($donnees = $tablebonbons->fetch())
					{
						if($donnees['categorie'] == $categorie)
						{
		?>

							<div id="bonbons">
								<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><img src="images/produits_petits/<?php echo $donnees['imagePetite']; ?>" alt=" <?php echo $donnees['nom']; ?> "/></a>

								<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><h3><?php echo $donnees['nom']; ?></h3></a>
							</div>
							<div id="prix">
								<p><?php echo $donnees['prix']; ?>/50g</p>
							
								<!--BOUTON !!! AJOUTER AU PANIER -->
								<a href="includes/ajoutpanier.php?numproduit=<?php echo $donnees['no']; ?>&typebonbon=<?php echo $_GET['typebonbon']; ?>&pageprecedente=produits" class="btn" id="aj<?php echo $donnees['no']; ?>">Ajouter au panier</a>
								
								<?php
									if(isset($_GET['rupture']) AND isset($_GET['numproduit']))
									{
										if($_GET['numproduit'] == $donnees['no'])
										{
											echo '<p><strong>Rupture de stock</strong></p>';
										}
									}
								?>
							</div>
				
							
		<?php
						}
					}
				}
				else
				{
					echo "<h2>Tous les bonbons</h2>";
					?>
					<div class="souligne"></div>
					<?php
				while($donnees = $tablebonbons->fetch())
					{
		?>
						<div id="bonbons">
							<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><img src="images/produits_petits/<?php echo $donnees['imagePetite']; ?>" alt=" <?php echo $donnees['nom']; ?> "/></a>

							<a href="descriptionproduit.php?numproduit=<?php echo $donnees['no']; ?> "><h3><?php echo $donnees['nom']; ?></h3></a>
						</div>
						<div id="prix">
								<p><?php echo $donnees['prix']; ?>/50g</p>
								
								<!--BOUTON !!! AJOUTER AU PANIER -->
								<a href="includes/ajoutpanier.php?numproduit=<?php echo $donnees['no']; ?>&pageprecedente=produits" class="btn" id="aj<?php echo $donnees['no']; ?>">Ajouter au panier</a>								
								
								<?php
									if(isset($_GET['rupture']) AND isset($_GET['numproduit']))
									{
										if($_GET['numproduit'] == $donnees['no'])
										{
											echo '<p><strong>Rupture de stock</strong></p>';
										}
									}
								?>
						</div>
					
							
							
		<?php
					}
				}
			}
			?>
			</section><!-- Fin de la section "contenu" -->
		</div>
		<!-- Pied de page -->
		<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
			
	<!-- script ajax -->
	<script src="js/recherche.js"></script>
		<script src="js/produit.js"></script>
</body>
</html>
