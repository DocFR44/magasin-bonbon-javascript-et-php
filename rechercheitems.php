<?php
header("Content-type: application/json; charset=utf-8");
header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header("Pragma: no-cache");

$msgErreur = null;
if ( ! isset($_GET["recherche"]) )
	$msgErreur = "Le paramètre \"recherche\" n'a pas été fourni avec la requête.";
else
{

	require("includes/param_bd.inc");

	try {
		$connBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$connBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	} catch (PDOException $e) {
		exit( "Erreur lors de la connexion à la BD :<br />\n" .  $e->getMessage() );
	}

	$rech = $_GET["recherche"];

	try {
		$req = "SELECT * FROM produits WHERE nom LIKE :champRecherche OR description LIKE :champRecherche";
		$prepReq = $connBD -> prepare($req);
		$prepReq -> execute( array( "champRecherche" => '%'.$rech.'%') );
		$prepReq -> setFetchMode(PDO::FETCH_OBJ);
	} catch (PDOException $e) {
		exit( "Erreur lors de l'exécution de la requête SQL :<br />\n" .  $e -> getMessage() . "<br />\nREQUÊTE = " . $req );
	}

	
	if ($prepReq->rowCount() == 0)
		$msgErreur = "Cet article n'existe pas.";
	else
	{
		
		$lstPersonnes = "[\n";
		while($info = $prepReq -> fetch())
		{
			$nom = $info -> nom;
			$no = $info -> no;
			$categorie = $info -> categorie;
			$nbrVendu = 0;
		try {
			$req2 = "SELECT * FROM items_commande WHERE items_commande.noProduit = :noRecherche";
			$prepReq2 = $connBD -> prepare($req2);
			$prepReq2 -> execute(array("noRecherche" =>$no));
			$prepReq2 -> setFetchMode(PDO::FETCH_OBJ);
			} catch (PDOException $e) 
			{
				exit( "Erreur lors de l'exécution de la requête SQL :<br />\n" .  $e -> getMessage() . "<br />\nREQUÊTE = " . $req2 );
			}

			while($infoItem = $prepReq2 -> fetch())
			{
				$nbrVendu += $infoItem -> qte;
			}
			
			$lstPersonnes .= "\t{\n";
			$lstPersonnes .= "\t\t\"nom\": \"$nom\",\n";
			$lstPersonnes .= "\t\t\"no\": \"$no\",\n";
			$lstPersonnes .= "\t\t\"categorie\": \"$categorie\",\n";
			$lstPersonnes .= "\t\t\"NombreVendu\": \"$nbrVendu\"\n";
			$lstPersonnes .= "\t},\n";
		}
		$lstPersonnes .= "]";
		
		$posDerniereVirgule = strrpos($lstPersonnes,",");
		
		$lstPersonnes = substr_replace($lstPersonnes,"",$posDerniereVirgule,1);
		
		echo $lstPersonnes;
	}

	$prepReq -> closeCursor();

	$connBD = null;
}


if ( $msgErreur != null )
{
		echo "{\n";
		echo "\t\"erreur\":\n";
		echo "\t{\n";
			echo "\t\t\"message\": \"" . str_replace("\"", "\\\"", $msgErreur) . "\"\n";
		echo "\t}\n";
		echo "}\n";
}
?>