var xhr;
var items;
window.addEventListener('DOMContentLoaded', init, false);

function init() {
	
	$('recherche').addEventListener('keyup', afficherInfoItemsAjax, false);
	$('recherche').addEventListener('click', afficherListeItems, false);
	
	$('listeItems').className = "afficher";
}

function afficherListeItems()
{
	$('listeItems').className = "afficher";	
}
	
function afficherInfoItemsAjax() 
{
	
	if($('recherche').value.length >= 3)
	{

		$('chargement').style.visibility = 'visible';

		if(xhr)
			xhr.abort();
		

		xhr = new XMLHttpRequest();


		xhr.addEventListener('readystatechange',afficherInfoItemsAjaxCallback,false);
		

		var URL = 'rechercheitems.php?recherche=' + encodeURIComponent($('recherche').value.trim());
		

		xhr.open('GET', URL, true);

		xhr.send(null);
	}
	else
	{
		var divListe = $('listeItems');
		while (divListe.firstChild) divListe.removeChild(divListe.firstChild);
		document.getElementById("listeItems").setAttribute("class", "cacher");
	}

}


function afficherInfoItemsAjaxCallback() 
{

	if ( xhr.readyState == 4 ) 
	{

		$('chargement').style.visibility = 'hidden';

		$('recherche').disabled = false;

		var erreur = false;

		if ( xhr.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr.status + '): La requête HTTP n\'a pu être complétée.';
			$('msg-erreur').textContent = msgErreur;
			erreur = true;

		} else {

			try { 
				items = JSON.parse( xhr.responseText );
			} catch (e) {
				alert('ERREUR: La réponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) {

				if ( items.erreur ) 
				{

					var msgErreur = items.erreur.message;
					$('msg-erreur').textContent = msgErreur;
					document.getElementById("listeItems").setAttribute("class", "cacher");

					while ($('listeItems').firstChild) $('listeItems').removeChild($('listeItems').firstChild);
					
				} else 
				{
					var contienttab = new Array();

					var tabJujube = new Array();
					var tabBonBonDur = new Array();
					var tabReglisse = new Array();
				
					var nbrJujube = 0;
					var nbrBonbonDur = 0;
					var nbrReglisse = 0;
					
					for (i=0;i<items.length;i++) 
					{
						switch(items[i].categorie) 
						{
						case "1":
							nbrJujube += parseInt(items[i].NombreVendu);
							tabJujube.push(items[i]);
							break;
						case "2":
							nbrBonbonDur += parseInt(items[i].NombreVendu);
							tabBonBonDur.push(items[i]);
							break;
						case "3":
							nbrReglisse += parseInt(items[i].NombreVendu);
							tabReglisse.push(items[i]);
							break;
						}
					}
					
					var contientOrdre = [
					{nom: "Jujube", nombre : nbrJujube}, 
					{nom: "Bonbondur" ,nombre:nbrBonbonDur}, 
					{nom: "Reglisse", nombre: nbrReglisse}];
					contientOrdre.sort(comparecategorie);

					tabJujube.sort(compare);
					tabBonBonDur.sort(compare);
					tabReglisse.sort(compare);
					
					for (i=0;i<3;i++) 
					{
						switch(contientOrdre[i].nom) 
						{
						case "Jujube":
							contienttab.push(tabJujube);
							break;
						case "Bonbondur":
							contienttab.push(tabBonBonDur);
							break;
						case "Reglisse":
							contienttab.push(tabReglisse);
							break;
						}
					}
					$('msg-erreur').textContent = "";
					var divListe = $('listeItems');

					while (divListe.firstChild) divListe.removeChild(divListe.firstChild);
					


					for (j=0;j<3;j++) 
					{
						if (contienttab[j].length > 0)
						{
							document.getElementById("listeItems").setAttribute("class", "cacher");
							var nomCategorie = document.createElement('p');
							nomCategorie.setAttribute('id', "titreCategorie");
							nomCategorie.innerHTML = contientOrdre[j].nom;
							divListe.appendChild(nomCategorie);
							var elemListe = document.createElement('ul');
							elemListe.setAttribute('id', "listeCategorie");
							divListe.appendChild(elemListe);
							
						}
						for (i=0;i<contienttab[j].length;i++) 
						{
							var elem = document.createElement('li');
							elem.textContent = contienttab[j][i].nom;

							elem.setAttribute('id', contienttab[j][i].no);
							elem.style.cursor = "pointer";
							elem.addEventListener('click',selectionneItem,false);
							
							elemListe.appendChild(elem);
						}
					}
					document.getElementById("listeItems").setAttribute("class", "montrer");
				}
			}
		}
	}
}

function selectionneItem(e)
{
	window.location.href = "descriptionproduit.php?numproduit=" + e.target.id;
}

function compare(a,b) 
{
  if (a.NombreVendu > b.NombreVendu)
	return -1;
  if (a.NombreVendu < b.NombreVendu)
	return 1;
  return 0;
}
function comparecategorie(a,b) 
{
  if (a.nombre > b.nombre)
	return -1;
  if (a.nombre < b.nombre)
	return 1;
  return 0;
}