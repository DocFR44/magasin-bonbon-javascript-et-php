window.addEventListener('DOMContentLoaded', init, false);

var xhr;
var msg;

var submitClick = false;
var regNom = /^[a-zA-Zéèçàê-]{1,50}$/;
var regPrenom = /^[a-zA-Zéèçàê-]{1,50}$/;
var regVille = /^[a-zA-Zéèçàê-]{1,50}$/;
var regCodePostal = /^[A-Z]{1}[0-9]{1}[A-Z]{1}\s[0-9]{1}[A-Z]{1}[0-9]{1}$/;
var regCourriel = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,4}$/;
var regMotPasse =/^[a-zA-Z0-9]{1,40}$/;
var regAdresse = /./;

var login = /^[a-zA-Z0-9]{1,20}$/;
var msgErreur = document.getElementById("messageErreur");

function init()
{
	document.getElementById('login').addEventListener('blur', afficherMsgAjax, false);
	document.getElementById("formInscription").addEventListener("submit", valider, false);
}

function validerBlur(e)
{
	var reg;
	switch (e.target.id)
	{
		case "Nom":
			reg = regNom;
			break;
		case "Prenom":
			reg = regPrenom;
			break;
		case "Adresse":
			reg = regAdresse;
			break;
		case "Ville":
			reg= regVille;
			break;
		case "CodePostal":
			reg = regCodePostal;
			break;
		case "Courriel":
			reg = regCourriel;
			break;
		case "passe":
			reg = regMotPasse;
			break;
	}

	if(!reg.test(document.getElementById(e.target.id).value))
	{
		document.getElementById(e.target.id).className = "errChamps";
	}
	else
	{
		document.getElementById(e.target.id).className = "pasErreur";
	}
}

function valider(e)
{
	document.getElementById("Nom").value = document.getElementById('Nom').value.trim();
	document.getElementById('Prenom').value = document.getElementById('Prenom').value.trim();
	document.getElementById('Adresse').value = document.getElementById('Adresse').value.trim();
	document.getElementById('Ville').value = document.getElementById('Ville').value.trim();
	document.getElementById('CodePostal').value = document.getElementById('CodePostal').value.trim();
	document.getElementById('Courriel').value = document.getElementById('Courriel').value.trim();
	document.getElementById('passe').value = document.getElementById('passe').value.trim();
	document.getElementById('login').value = document.getElementById('login').value.trim();

	while (msgErreur.firstChild) msgErreur.removeChild(msgErreur.firstChild);
	var erreur = false;
	var creationListe = document.createElement('ul');
	msgErreur.appendChild(creationListe);
	if(!(regNom.test(document.getElementById("Nom").value)) || document.getElementById("Nom").value == "")
	{
		document.getElementById("Nom").className = "errChamps";
		var creationNom = document.createElement('li');
		creationNom.textContent = "Ce nom est incorrect";
		creationListe.appendChild(creationNom);
		erreur = true;
	}
	else
	{
		document.getElementById("Nom").className = "pasErreur";
	}

	if(!(regPrenom.test(document.getElementById("Prenom").value)) || document.getElementById("Prenom").value == "")
	{
		document.getElementById("Prenom").className = "errChamps";
		var creationPrenom = document.createElement('li');
		creationPrenom.textContent = "Ce prénom est incorrect";
		creationListe.appendChild(creationPrenom);
		erreur = true;
	}
	else
	{
		document.getElementById("Prenom").className = "pasErreur";
	}

	if(document.getElementById("Adresse").value == "")
	{
		document.getElementById("Adresse").className = "errChamps";
		var creationAdresse = document.createElement('li');
		creationAdresse.textContent = "Veuillez entrer une adresse";
		creationListe.appendChild(creationAdresse);
		erreur = true;
	}
	else
	{
		document.getElementById("Adresse").className = "pasErreur";
	}

	if(!(regVille.test(document.getElementById("Ville").value)) || document.getElementById("Ville").value == "")
	{
		document.getElementById("Ville").className = "errChamps";
		var creationVille = document.createElement('li');
		creationVille.textContent = "Ce nom  de ville est incorrect";
		creationListe.appendChild(creationVille);
		erreur = true;
	}
	else
	{
		document.getElementById("Ville").className = "pasErreur";
	}

	if(!(regCodePostal.test(document.getElementById("CodePostal").value)) || document.getElementById("CodePostal").value == "")
	{
		document.getElementById("CodePostal").className = "errChamps";
		var creationCodePostal = document.createElement('li');
		creationCodePostal.textContent = "Vous devez entrer le code postal comme ceci : A1A 1A1";
		creationListe.appendChild(creationCodePostal);
		erreur = true;
	}
	else
	{
		document.getElementById("CodePostal").className = "pasErreur";
	}

	if(!(regCourriel.test(document.getElementById("Courriel").value)) || document.getElementById("Courriel").value == "")
	{
		document.getElementById("Courriel").className = "errChamps";
		var creationCourriel = document.createElement('li');
		creationCourriel.textContent = "Vous devez entrer un courriel valide";
		creationListe.appendChild(creationCourriel);
		erreur = true;
	}
	else
	{
		document.getElementById("Courriel").className = "pasErreur";
	}

	if(!(regMotPasse.test(document.getElementById("passe").value)) || document.getElementById("passe").value == "")
	{
		document.getElementById("passe").className = "errChamps";
		var creationPasse = document.createElement('li');
		creationPasse.textContent = "Vous devez entrer un mot de passe valide.";
		creationListe.appendChild(creationPasse);
		erreur = true;
	}
	else
	{
		document.getElementById("passe").className = "pasErreur";
	}

	if(document.getElementById("login").value == "")
	{
		document.getElementById("login").className = "errChamps";
		var creationLogin = document.createElement('li');
		creationLogin.textContent = "Veuillez entrer un login";
		creationListe.appendChild(creationLogin);
		erreur = true;
	}
	else
	{
		document.getElementById("login").className = "pasErreur";
	}

	if(erreur)
	{
		e.preventDefault();
	}

	document.getElementById('Nom').addEventListener('blur', validerBlur, false);
	document.getElementById('Prenom').addEventListener('blur', validerBlur, false);
	document.getElementById('Adresse').addEventListener('blur', validerBlur, false);
	document.getElementById('Ville').addEventListener('blur', validerBlur, false);
	document.getElementById('CodePostal').addEventListener('blur', validerBlur, false);
	document.getElementById('Courriel').addEventListener('blur', validerBlur, false);
	document.getElementById('passe').addEventListener('blur', validerBlur, false);

	
}

function afficherMsgAjax() 
{

	
	if(xhr)
	{
	xhr.abort();
	}
	
	xhr = new XMLHttpRequest();
	xhr.addEventListener('readystatechange', afficherMsg,false);
	var URL = 'verfierlogindispo.php?login=' + encodeURIComponent($('login').value.trim());
	xhr.open('GET', URL, true);
	xhr.send(null);

}

function afficherMsg(e) 
{
	if ( xhr.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr.status + '): La requête HTTP n\'a pu être complétée.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				msg = JSON.parse( xhr.responseText );
			} catch (e) 
			{
				alert('ERREUR: La réponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{
				if ( msg.erreur ) 
				{
					var msgErreur = 'Erreur: ' + msg.erreur.message;
					$('erreurLogin').textContent = msgErreur;
									
				} else 
				{
					if(msg.message)
					{
						document.getElementById("login").className = "pasErreur";
					}
					else
					{
						document.getElementById("login").className = "errChamps";
						var creationLogin = document.createElement('li');
						creationLogin.textContent = "Veuillez entrer un login";
						creationListe.appendChild(creationPasse);
						erreur = true;
					}					
				}
			}
		}
		if(erreur)
		{
			e.preventDefault();
		}
	}
}
