window.addEventListener('load',init,false);


// Tableau des noms de fichiers pour les images avec l'extension (sans le dossier)
var tabNomsFichiers = ["img0.png", "img1.png", "img2.png"];

// Délai entre les images en millisecondes
var delai = 2000;

// Indice de la première image affichée
var indiceImage = 0;

// Minuterie pour l'animation
var minAnimation;
function init()
{
	 document.getElementById("images_promotions").addEventListener("click",redirectionDePage,false);
	 
	 for (var i = 0;i <= 2; i++)
	 {
	  document.getElementById("img" + i).addEventListener("mouseover",afficherImagePromotion,false);
	  document.getElementById("img" + i).addEventListener('mouseout',demarrerVisionneuse,false);
	 }

	demarrerVisionneuse();
	window.addEventListener('unload',stopTimer,false);
	

}
function redirectionDePage()
{
		switch(indiceImage) 
	{
    case 0:
		window.location.href = "descriptionproduit.php?numproduit=4"
        break;
    case 1:
		window.location.href = "produits.php"
        break;
	case 2:
		window.location.href = "descriptionproduit.php?numproduit=2"
        break;
	}
}
function afficherImagePromotion(e)
{

	document.getElementById("images_promotions").src = "images/images_promotions/" + e.target.id + ".png";
	document.getElementById(e.target.id).src = "images/images_promotions/pico2.png";
	window.clearInterval(minAnimation);
	
	switch(e.target.id) 
	{
    case "img0":
        indiceImage = 0;
		document.getElementById("img1").src = "images/images_promotions/pico1.png";
		document.getElementById("img2").src = "images/images_promotions/pico1.png";
        break;
    case "img1":
        indiceImage = 1;
		document.getElementById("img0").src = "images/images_promotions/pico1.png";
		document.getElementById("img2").src = "images/images_promotions/pico1.png";
        break;
	case "img2":
        indiceImage = 2;
		document.getElementById("img0").src = "images/images_promotions/pico1.png";
		document.getElementById("img1").src = "images/images_promotions/pico1.png";
        break;
	}

}
	
// Fonction démarrant l'animation
// La méthode setInterval() appelle une fonction et la rappelle après un certain delai, tant et aussi longtemps que la méthode
// clearInterval() n'est pas appelée ou que la fenêtre n'est pas fermée. La valeur retournée par setInterval() est utilisée 
// comme paramètre pour la méthode clearInterval() .
function demarrerVisionneuse()
{
	// Début de l'animation
	minAnimation = window.setInterval(changerImage, delai);

}

// Fonction changeant l'image
function changerImage()
{
	
	document.getElementById("img" + indiceImage).src = "images/images_promotions/pico1.png";

	indiceImage = (indiceImage+1) % tabNomsFichiers.length;
	document.getElementById("img" + indiceImage).src = "images/images_promotions/pico2.png";
	document.images["images_promotions"].src  = "images/images_promotions/" + tabNomsFichiers[indiceImage];
	

}

// Fonction arrêtant l'animation
function stopTimer()
{
	// Arrêt de l'animation
	window.clearInterval(minAnimation);
}