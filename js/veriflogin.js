
var xhr;
var msg;
window.addEventListener('DOMContentLoaded', init, false);

function init() 
{
	document.getElementById('login').addEventListener('blur', afficherMsgAjax, false);

}

function afficherMsgAjax() 
{

	
	if(xhr)
	{
	xhr.abort();
	}
	
	xhr = new XMLHttpRequest();
	xhr.addEventListener('readystatechange', afficherMsg,false);
	var URL = 'verfierlogindispo.php?login=' + encodeURIComponent($('login').value.trim());
	xhr.open('GET', URL, true);
	xhr.send(null);

}

function afficherMsg() 
{
	if ( xhr.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr.status + '): La requ�te HTTP n\'a pu �tre compl�t�e.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				msg = JSON.parse( xhr.responseText );
			} catch (e) 
			{
				alert('ERREUR: La r�ponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{
				if ( msg.erreur ) 
				{
					var msgErreur = 'Erreur: ' + msg.erreur.message;
					$('erreurlogin').textContent = msgErreur;					
				} else 
				{
				
					document.getElementById("passe").className = "errChamps";
					
				}
			}
		}
	}
}