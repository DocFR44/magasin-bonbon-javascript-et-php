var xhr2;
var xhr3;
var xhr4;
var xhr5;
var items2;
var itemModif;
window.addEventListener('DOMContentLoaded', init, false);
var delaiPanier = 5000;
var timerpanier;
var prixtotal;

function init() {
	
	$('panierjs').addEventListener('mouseover', afficherInfoItemsAjax2, false);

	
}


function afficherInfoItemsAjax2() 
{
		if(xhr2)
			xhr2.abort();
		

		xhr2 = new XMLHttpRequest();


		xhr2.addEventListener('readystatechange',afficherInfoItemsAjax2Callback,false);
		

		var URL = 'rechercheitemspanier.php';
		

		xhr2.open('GET', URL, true);

		xhr2.send(null);
}


function afficherInfoItemsAjax2Callback() 
{
	window.clearInterval(timerpanier);
	if ( xhr2.readyState == 4 ) 
	{
		var erreur = false;

		if ( xhr2.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr2.status + '): La requête HTTP n\'a pu être complétée.';
			$('msg-erreur').textContent = msgErreur;
			erreur = true;
		} else {

			try { 
				items2 = JSON.parse( xhr2.responseText );
			} catch (e) {
				erreur = true;
			}

			if (!erreur) 
			{


				var divListe = $('petitPanier');

				while (divListe.firstChild) divListe.removeChild(divListe.firstChild);
				prixtotal = 0;
				
				for (i=0;i<items2.length;i++)
				{
					prixtotal += parseFloat(items2[i].prix) * parseInt(items2[i].quantite);
					var divItem = document.createElement('div');
					divItem.setAttribute('class', "ItemPetitPanier");
					divItem.setAttribute('id', "e"+items2[i].no);

					var divimgNom = document.createElement('div');
					divimgNom.setAttribute('id', "ImageNom");

					var imgItem = document.createElement('img');
					imgItem.src = "images/produits_minis/" + items2[i].image;
					imgItem.setAttribute('class', items2[i].no);
					imgItem.setAttribute('id', "g"+items2[i].no);
					imgItem.addEventListener('click', infoProduit,false);
					imgItem.style.cursor = "pointer";
					
					var nomItem = document.createElement('p');
					nomItem.innerHTML = items2[i].nom;
					nomItem.setAttribute('class', items2[i].no);
					nomItem.setAttribute('id', "g"+items2[i].no);
					nomItem.addEventListener('click', infoProduit,false);
					nomItem.style.cursor = "pointer";
					
					var divPrixQte = document.createElement('div');
					divPrixQte.setAttribute('id', "prixqte");

					var prixItem = document.createElement('p');
					prixItem.innerHTML = parseFloat(items2[i].prix) * parseInt(items2[i].quantite) + '$';
					prixItem.setAttribute('class', "petitPanierPrix");
					prixItem.setAttribute('id', "a"+items2[i].no);
					
					var ajouterUn = document.createElement('p');
					ajouterUn.innerHTML = "+";
					ajouterUn.setAttribute('class', "btn");
					ajouterUn.setAttribute('id', "i"+items2[i].no);
					ajouterUn.addEventListener('click', ajouterAuPanier,false);
					ajouterUn.style.cursor = "pointer";
					
					var qteItem = document.createElement('p');
					qteItem.innerHTML = parseInt(items2[i].quantite) * 50 + 'g';
					qteItem.setAttribute('class', "petitPanierQuantite");
					qteItem.setAttribute('id', "q"+items2[i].no);
					
					var enleverUn = document.createElement('p');
					enleverUn.innerHTML = "-";
					enleverUn.setAttribute('class', "btn");
					enleverUn.setAttribute('id', "i"+items2[i].no);
					enleverUn.addEventListener('click', enleverPanier,false);
					enleverUn.style.cursor = "pointer";
					
					var enleverItem = document.createElement('p');
					enleverItem.innerHTML = "X";
					enleverItem.setAttribute('class', "btn");
					enleverItem.setAttribute('id', "i"+items2[i].no);
					enleverItem.addEventListener('click', enleverTout,false);
					enleverItem.style.cursor = "pointer";
					

					divListe.appendChild(divItem);

					divItem.appendChild(divimgNom);
					divItem.appendChild(divPrixQte);

					divimgNom.appendChild(imgItem);
					divimgNom.appendChild(nomItem);

					divPrixQte.appendChild(prixItem);
					divPrixQte.appendChild(enleverUn);
					divPrixQte.appendChild(qteItem);
					divPrixQte.appendChild(ajouterUn);
					divPrixQte.appendChild(enleverItem);
				}
					var prixfinal = document.createElement('p');
					prixfinal.innerHTML = "total: " + prixtotal + '$';
					prixfinal.setAttribute('id', "prixFinal");
					divListe.appendChild(prixfinal);
					
					var allerpanier = document.createElement('p');
					allerpanier.innerHTML = "Aller au panier";
					divListe.appendChild(allerpanier);
					allerpanier.addEventListener('click', allerAuPanier,false);
					allerpanier.style.cursor = "pointer";
					allerpanier.setAttribute('id', "allerpanier");
					
					timerpanier = window.setInterval(enleverElemPanier, delaiPanier);

				document.getElementById("panierjs").innerHTML = "panier("+ (divListe.childElementCount - 2) +")";
			}
			else
			{
				var divListe = $('petitPanier');
				while (divListe.firstChild) divListe.removeChild(divListe.firstChild);
				var msgPasItem = document.createElement('p');
				msgPasItem.innerHTML = "Panier Vide";
				divListe.appendChild(msgPasItem);

				document.getElementById("panierjs").innerHTML = "panier("+ 0 +")";
				timerpanier = window.setInterval(enleverElemPanier, delaiPanier);
			}
			document.getElementById("petitPanier").setAttribute("class", "montrer");
		}
	}
}
function allerAuPanier()
{
	window.location.href = "panier.php";
}
function ajouterAuPanier(e)
{
	var noId = "";
	var leId = e.target.id;
	var leNo = leId.split('');
	for(var i = 1; i < leNo.length; i++)
	{
		noId += leNo[i];
	}
	
		if(xhr3)
			xhr3.abort();
		

		xhr3 = new XMLHttpRequest();


		xhr3.addEventListener('readystatechange', modifQteAjoute,false);
		

		var URL2 = 'ajoutpanierajax.php?numproduit=' + noId;

		xhr3.open('GET', URL2, true);

		xhr3.send(null);
}
function modifQteAjoute()
{
	if ( xhr3.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr3.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr3.status + '): La requête HTTP n\'a pu être complétée.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				itemModif = JSON.parse( xhr3.responseText );
			} catch (e) 
			{
				alert('ERREUR: La réponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{
				document.getElementById("q"+itemModif[0].no).innerHTML = parseInt(itemModif[0].quantite) * 50 + "g";
				document.getElementById("a"+itemModif[0].no).innerHTML = parseInt(itemModif[0].quantite)  * parseFloat(itemModif[0].prix) + "$";
				document.getElementById("prixFinal").innerHTML = (prixtotal + parseFloat(itemModif[0].prix)) + "$";
			}
		}
	}
}

function enleverPanier(e)
{
	var noId = "";
	var leId = e.target.id;
	var leNo = leId.split('');
	for(var i = 1; i < leNo.length; i++)
	{
		noId += leNo[i];
	}
	
		if(xhr4)
			xhr4.abort();	

		xhr4 = new XMLHttpRequest();

		xhr4.addEventListener('readystatechange', modifQteEnlever,false);
		

		var URL2 = 'enleverpanierajax.php?numproduit=' + noId;

		xhr4.open('GET', URL2, true);

		xhr4.send(null);
}
function modifQteEnlever()
{
	if ( xhr4.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr4.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr4.status + '): La requête HTTP n\'a pu être complétée.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				itemModif = JSON.parse( xhr4.responseText );
			} catch (e) 
			{
				alert('ERREUR: La réponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{
				var divListe = $('petitPanier');
				if(itemModif[0].quantite == 0)
				{
					var elem = document.getElementById("e" +itemModif[0].no);
					divListe.removeChild(elem);
					document.getElementById("panierjs").innerHTML = "panier("+ (divListe.childElementCount - 2) +")";
				}
				else
				{
				document.getElementById("q"+itemModif[0].no).innerHTML = parseInt(itemModif[0].quantite) * 50 + "g";
				document.getElementById("a"+itemModif[0].no).innerHTML = parseInt(itemModif[0].quantite)  * parseFloat(itemModif[0].prix) + "$";
				}
				if(divListe.childElementCount > 2)
				{
				document.getElementById("prixFinal").innerHTML = (prixtotal - parseFloat(itemModif[0].prix)) + "$";
				}
				else
				{
					var elemPrixTotal = document.getElementById("prixFinal");
					divListe.removeChild(elemPrixTotal);
					document.getElementById("allerpanier").innerHTML = "Le panier est vide";
					document.getElementById("panierjs").innerHTML = "panier("+ 0 +")";
				}
			}
		}
	}
}
function enleverTout(e)
{
	var noId = "";
	var leId = e.target.id;
	var leNo = leId.split('');
	for(var i = 1; i < leNo.length; i++)
	{
		noId += leNo[i];
	}

	
		if(xhr5)
			xhr5.abort();	

		xhr5 = new XMLHttpRequest();

		xhr5.addEventListener('readystatechange', modifQteEnleverTout,false);
		

		var URL2 = 'enleverpanierajax.php?numproduit=' +noId+"&enleverbonbon=un";

		xhr5.open('GET', URL2, true);

		xhr5.send(null);
}
function modifQteEnleverTout()
{
	if ( xhr5.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr5.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr5.status + '): La requête HTTP n\'a pu être complétée.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				itemModif = JSON.parse( xhr5.responseText );
			} catch (e) 
			{
				alert('ERREUR: La réponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{
				var divListe = $('petitPanier');
				
				var elemDivBonbon = document.getElementById("e"+itemModif[0].no);
				divListe.removeChild(elemDivBonbon);
				document.getElementById("panierjs").innerHTML = "panier("+ (divListe.childElementCount - 2) +")";
				if(divListe.childElementCount > 2)
				{
					document.getElementById("prixFinal").innerHTML = (prixtotal - (parseFloat(itemModif[0].prix) * parseFloat(itemModif[0].quantite))) + "$";
				}
				else
				{
					var elemPrixTotal = document.getElementById("prixFinal");
					divListe.removeChild(elemPrixTotal);
					document.getElementById("allerpanier").innerHTML = "Le panier est vide";
					document.getElementById("panierjs").innerHTML = "panier("+ 0 +")";
				}
			}
		}
	}
}
function infoProduit(e)
{
	var noId = "";
	var leId = e.target.id;
	var leNo = leId.split('');
	for(var i = 1; i < leNo.length; i++)
	{
		noId += leNo[i];
	}
	window.location.href = "descriptionproduit.php?numproduit=" + noId;

}

function enleverElemPanier()
{
	var divListe = $('petitPanier');
	while (divListe.firstChild) divListe.removeChild(divListe.firstChild);
	window.clearInterval(timerpanier);
	document.getElementById("petitPanier").setAttribute("class", "cacher");

}
function selectionneItem(e)
{
	window.location.href = "descriptionproduit.php?numproduit=" + e.target.id;
}