
var itemModif2;
window.addEventListener('DOMContentLoaded', init, false);

function init() 
{
var tabBoutons = document.getElementsByClassName("btn");

	for (var i = 0;i < tabBoutons.length; i++)
	{
		tabBoutons[i].addEventListener("click",ajouterAuPanierPageProduit,false);
	}

}
function ajouterAuPanierPageProduit(e)
{

	var noId = "";
	var leId = e.target.id;
	var leNo = leId.split('');
	for(var i = 2; i < leNo.length; i++)
	{
		noId += leNo[i];
	}
	
		if(xhr2)
			xhr2.abort();
		

		xhr2 = new XMLHttpRequest();


		xhr2.addEventListener('readystatechange', modifQteAjoutePageProduit,false);
		

		var URL2 = 'ajoutpanierajax.php?numproduit=' + noId;

		xhr2.open('GET', URL2, true);

		xhr2.send(null);
		
	e.preventDefault();
}
function modifQteAjoutePageProduit()
{
	if ( xhr2.readyState == 4 ) 
	{		
		var erreur = false;

		if ( xhr2.status != 200 ) 
		{
			var msgErreur = 'Erreur (code=' + xhr2.status + '): La requ�te HTTP n\'a pu �tre compl�t�e.';
			$('erreurlogin').textContent = msgErreur;
			erreur = true;
		} 
		else 
		{
			try 
			{ 
				itemModif2 = JSON.parse( xhr2.responseText );
			} catch (e) 
			{
				alert('ERREUR: La r�ponse AJAX n\'est pas une expression JSON valide.');
				erreur = true;
			}

			if (!erreur) 
			{

					if(xhr2)
						xhr2.abort();
					

					xhr2 = new XMLHttpRequest();


					xhr2.addEventListener('readystatechange',afficherInfoItemsAjax2Callback,false);
					

					var URL = 'rechercheitemspanier.php';
					

					xhr2.open('GET', URL, true);

					xhr2.send(null);
			}
		}
	}
}