<!-- CETTE PAGE EST SÉCURISÉE SSL (https)!-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | commande</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		//<!-- Menu principal -->

			include 'includes/menu.php';
			include 'includes/param_bd.inc';
		?>
		<!-- Contenu -->
		<section id="contenu">
		<h2>commander</h2><div class="souligne"></div>
			<?php
			if(isset($_POST['typepaiement']))
			{
				try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				try
				{
				$reqLireClient = $connexionBD->prepare('SELECT * FROM clients WHERE login = :login');
				$reqLireClient->execute(array('login' => $_SESSION['login']));
				$noclient=$reqLireClient->fetch();
				$reqLireClient->closecursor();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				try
				{
				$date = date('Y-m-d');
				$reqEcrire = $connexionBD->prepare('INSERT INTO commandes(date, statut, typePaiement, noClient) VALUES(:date, :statut, :typepaiement, :noClient)');
				$reqEcrire->execute(array('date' => $date, 'statut' => "En paiement", 'typepaiement' => $_POST['typepaiement'], 'noClient' =>$noclient['no']));
				$reqEcrire->closecursor();
	
				$numerocommande = $connexionBD->lastinsertid();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				
				foreach(array_keys($_SESSION['panier']) as $item)
				{
					try
					{
					$req = $connexionBD->prepare('INSERT INTO items_commande(noCommande, noProduit, qte) VALUES(:noCommande, :noProduit, :qte)');
					$req->execute(array('noCommande' => $numerocommande, 'noProduit' => $item, 'qte' => $_SESSION['panier'][$item]));
					$req->closecursor();
					}
					catch(Exception $e)
					{
						// En cas d'erreur, on affiche un message et on arrête tout
							die('Erreur : '.$e->getMessage());
					}

				}
				$connexionBD = null;
				unset($_SESSION['panier']);
				header('Location:confirmation.php?confirmer=payer');
				exit;
			}

			if (isset($_SESSION['login']))
			{
				try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				try
				{
				$reqLireClient = $connexionBD->prepare('SELECT * FROM clients WHERE login = :login');
				$reqLireClient->execute(array('login' => $_SESSION['login']));
				$noclient=$reqLireClient->fetch();
				$reqLireClient->closecursor();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				?>
				<div id="adresseLivraison">
					<h3>Adresse de livraison</h3>
					<p><?php echo  htmlspecialchars($noclient['prenom']).' '. htmlspecialchars($noclient['nom']) ?></p>
					<p><?php echo  htmlspecialchars($noclient['adresse']) ?></p>
					<p><?php echo  htmlspecialchars($noclient['ville']).' ('.$noclient['province'].')  '. htmlspecialchars($noclient['codePostal']) ?></p>
				</div>

				<form method="post" action="commande.php" id="paiement">
					<fieldset>
						<legend><h3>Mode de paiement:</h3></legend>
						<ul>
							<li>
								<input type="radio" name="typepaiement" id="visa" value="Visa" checked="checked" />
								<label for="visa">Visa</label>
							</li>
							<li>	
								<input type="radio" name="typepaiement" id="mastercard" value="Mastercard" />
								<label for="mastercard">Mastercard</label>
							</li>
							<li>
								<input type="radio" name="typepaiement" id="paypal" value="Paypal" />
								<label for="paypal">Paypal</label>
							</li>
						</ul>
					</fieldset>

								<?php
			$prixtotal = 0;
			if(!empty($_SESSION['panier']))
			{
				try
				{
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				?>
				<h3 class="commandeh3">Commande</h3>
				<?php
				foreach(array_keys($_SESSION['panier']) as $item)
				{
					try
					{
						$req = $connexionBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
						$req->execute(array('item'=>$item));
						
						$infoItem = $req->fetch();

						$req->closeCursor();
					}
					catch(Exception $e)
					{
						// En cas d'erreur, on affiche un message et on arrête tout
							die('Erreur : '.$e->getMessage());
					}

				?>

					<div id="elemPanier">
						<div id="imageNom">
							<a href="descriptionproduit.php?numproduit=<?php echo $infoItem['no']; ?> "><img src="images/produits_petits/<?php echo $infoItem['imagePetite']; ?>" alt=" <?php echo $infoItem['nom']; ?> "/></a>

							<a href="descriptionproduit.php?numproduit=<?php echo $infoItem['no']; ?> "><h4><?php echo $infoItem['nom']; ?></h4></a>
						</div>
						
						<div id="infoItemPanier">
							<div class="floaterGauche">
								<p><?php echo $infoItem['prix']; ?>$/50g</p>
							</div>

							<div class=floaterGauche>
								<p><?php echo $_SESSION['panier'][$item] * 50;?>g</p>
							</div>

							<div class="floaterGauche">
								<p><?php echo $_SESSION['panier'][$item] * $infoItem['prix'];?>$</p>
							</div>
						</div>
						<?php
						$prixtotal += ($_SESSION['panier'][$item] * $infoItem['prix']);
						?>
					</div>
				<?php
				}
				$connexionBD = null;
			}
			?>
				<p id="prixTotal">Prix total: <?php echo "$prixtotal";?>$</p>

				<p>
					<input type="submit" name="payer" id="payer" value="Payer" class="btn" />
				</p>
				</form>
				<?php
			}
			?>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
</body>
</html>
