<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>Em.gummy | erreur 404</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		?>
		<!-- Menu principal -->
					<?php
			include 'includes/menu.php';
		?>

		<!-- Contenue -->
		<section id="contenu">

			<h2 class="milieu">Cette page n'existe pas !</h2>
				
			<img src="images/confirmation.jpg" alt="Images de petit ours en jujube" id="confimationmilieu"/>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
</body>
</html>
