<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | panier</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		
		//<-- Menu principal -->

			include 'includes/menu.php';
			include 'includes/param_bd.inc';
		?>
		<!-- Contenu -->
		<section id="contenu">
			<h2>panier</h2><div class="souligne"></div>
			<?php
			$prixtotal = 0;
			if(!empty($_SESSION['panier']))
			{
				try
				{
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}

				?>

				<a href="includes/enleverpanier.php?enleverbonbon=tout" id="vider" class="btn" id="vider">Vider le panier</a><?php
				foreach(array_keys($_SESSION['panier']) as $item)
				{
					try
					{
						$req = $connexionBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
						$req->execute(array('item'=>$item));
						
						$infoItem = $req->fetch();

						$req->closeCursor();
					}
					catch(Exception $e)
					{
						// En cas d'erreur, on affiche un message et on arrête tout
							die('Erreur : '.$e->getMessage());
					}

				?>

					<div id="elemPanier">
						<div id="imageNom">
							<a href="descriptionproduit.php?numproduit=<?php echo $infoItem['no']; ?> "><img src="images/produits_petits/<?php echo $infoItem['imagePetite']; ?>" alt=" <?php echo $infoItem['nom']; ?> "/></a>

							<a href="descriptionproduit.php?numproduit=<?php echo $infoItem['no']; ?> "><h3><?php echo $infoItem['nom']; ?></h3></a>
						</div>
						
						<div id="infoItemPanier">
							<div class="floaterGauche">
								<p><?php echo $infoItem['prix']; ?>$/50g</p>
							</div>

							<div class="floaterGauche">
								<a href="includes/enleverpanier.php?numproduit=<?php echo $infoItem['no']; ?>" class="btn">-</a>
							</div>

							<div class=floaterGauche>
								<p><?php echo $_SESSION['panier'][$item] * 50;?>g</p>
							</div>

							<div class="floaterGauche">
								<a href="includes/ajoutpanier.php?numproduit=<?php echo $infoItem['no']; ?>&pageprecedente=panier" class="btn">+</a>
							</div>

							<div class="floaterGauche">
								<p><?php echo $_SESSION['panier'][$item] * $infoItem['prix'];?>$</p>
							</div>
						</div>

						<a href="includes/enleverpanier.php?numproduit=<?php echo $infoItem['no']; ?>&enleverbonbon=un" id="EnleverDuPanier" class="btn">X</a>

						<?php
							if(isset($_GET['rupture']) && isset($_GET['numproduit']))
							{
								if($_GET['numproduit'] == $infoItem['no'])
								{
									echo '<p><strong>Rupture de stock</strong></p>';
								}
							}
						$prixtotal += ($_SESSION['panier'][$item] * $infoItem['prix']);
						?>

					</div>
				<?php
				}
				$connexionBD = null;

				if(!(isset($_SESSION['login'])))
				{
				?>
				<form method="post" action="connexion.php?commande=vrai">
				<?php
				}
				else
				{
				?>
				<form method="post" action="commande.php">
				<?php
				}
				?>
				<p id="prixTotal">Prix total: <?php echo "$prixtotal";?>$</p>
				<input type="submit" value="Passer la commande" class="btn" id="passerCommande"/>

				</form>
				<?php
			}
			else
			{
				?>
				<p>Vous n'avez aucun articles dans le panier.</p>
				<?php
			}
			?>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
	<script type="text/javascript" src="js/validationPanier.js"></script>
</body>
</html>
