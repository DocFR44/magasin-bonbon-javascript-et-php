<!-- CETTE PAGE EST SÉCURISÉE SSL (https)!-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | inscription</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
			include 'includes/menu.php';
			include 'includes/param_bd.inc';

			$erreurNom = false;
			$erreurPrenom = false;
			$erreurVille = false;
			$erreurCodePostal = false;
			$erreurLogin = false;
			$erreurLoginExiste = false;
			$erreurCourriel = false;
			$erreurCourrielExiste = false;
			$erreurMotPasse = false;


			$modification = false;
		if(isset($_SESSION['login']))
		{
			$modification = true;
			try
			{
				// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			}
			catch(Exception $e)
			{
				// En cas d'erreur, on affiche un message et on arrête tout
					die('Erreur : '.$e->getMessage());
			}
			try
			{
				$reqLecture = $connexionBD->prepare('SELECT * FROM clients WHERE clients.login = :login');
				$reqLecture->execute(array('login'=>$_SESSION['login']));
				$infoClientEnligne = $reqLecture->fetch();
				$reqLecture->closeCursor();
				$connexionBD = null;
			}
			catch(Exception $e)
			{
				// En cas d'erreur, on affiche un message et on arrête tout
					die('Erreur : '.$e->getMessage());
			}

		}


		if(isset($_POST['login']))
		{
			try
			{
				// On se connecte à MySQL

				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			}
			catch(Exception $e)
			{
				// En cas d'erreur, on affiche un message et on arrête tout
					die('Erreur : '.$e->getMessage());
			}
				try
				{
					$req = $connexionBD->prepare('SELECT * FROM clients WHERE clients.login = :item');
					$req->execute(array('item'=>$_POST['login']));	
					$infoClient = $req->fetch();
					$req->closeCursor();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
						die('Erreur : '.$e->getMessage());
				}
			if(isset($infoClient['login']) && $modification == false)
			{
				$erreurLoginExiste = true;
			}
			else
			{
				try
				{
					$req2 = $connexionBD->prepare('SELECT * FROM clients WHERE clients.email = :item');
					$req2->execute(array('item'=>$_POST['Courriel']));	
					$infoClient2 = $req2->fetch();
					$req2->closeCursor();
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
						die('Erreur : '.$e->getMessage());
				}
				
				if (isset($infoClient2['email']) && $modification == false)
				{
					$erreurCourrielExiste = true;
				}
				else
				{
					if (!(preg_match("/^[a-zA-Zéèçàê-]{1,50}+$/", $_POST['Nom'])))
					{
						$erreurNom = true;
					}
					if (!(preg_match("/^[a-zA-Zéèçàê-]{1,50}+$/", $_POST['Prenom'])))
					{
						$erreurPrenom = true;
					}
					if (!(preg_match("/^[a-zA-Zéèçàê-]{1,50}+$/", $_POST['Ville'])))
					{
						$erreurVille = true;
					}
					if (!(preg_match("/^[A-Z]{1}[0-9]{1}[A-Z]{1}\s[0-9]{1}[A-Z]{1}[0-9]{1}+$/", $_POST['CodePostal'])))
					{
						$erreurCodePostal = true;
					}
					if ((!(preg_match("/^[a-zA-Z0-9]{1,20}+$/", $_POST['login']))) && ($modification == false))
					{
						$erreurLogin = true;
					}
					if (!(preg_match("#^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,4}+$#", $_POST['Courriel'])))
					{
						$erreurCourriel = true;
					}
					if ((!(preg_match("/^[a-zA-Z0-9]{1,40}+$/", $_POST['passe']))) && ($modification == false))
					{
						$erreurMotPasse = true;
					}
					if($modification)
					{
						if(($erreurNom == false && $erreurPrenom == false && $erreurVille == false && $erreurCodePostal == false && $erreurLogin == false && $erreurLoginExiste == false && $erreurCourriel == false && $erreurCourrielExiste == false && $erreurMotPasse == false))
						{
							try
							{
								$reqModif = $connexionBD->prepare('UPDATE clients SET nom = :nom, prenom = :prenom, adresse = :adresse, ville = :ville, province = :province, codePostal = :codePostal, email = :email WHERE no = :no'); 
								$reqModif->execute(array('nom' => $_POST['Nom'], 'prenom' => $_POST['Prenom'], 'adresse' => $_POST['Adresse'], 'ville' => $_POST['Ville'], 'province' => $_POST['Province'], 'codePostal' => $_POST['CodePostal'], 'email' => $_POST['Courriel'], 'no'=> $infoClientEnligne['no']));
								$reqModif->closeCursor();
							}
							catch(Exception $e)
							{
								// En cas d'erreur, on affiche un message et on arrête tout
									die('Erreur : '.$e->getMessage());
							}

							header('Location:confirmation.php?confirmer=modifie');
							exit;
						}
					}
					else
					{
						if(($erreurNom == false && $erreurPrenom == false && $erreurVille == false && $erreurCodePostal == false && $erreurLogin == false && $erreurLoginExiste == false && $erreurCourriel === false && $erreurCourrielExiste == false && $erreurMotPasse == false))
						{
							$mdp = hash('sha256',$_POST['passe']);
							$reqEcrire = $connexionBD->prepare("INSERT INTO clients(nom, prenom, adresse, ville, province, codePostal, login, motPasse, email) VALUES(:nom, :prenom, :adresse, :ville, :province, :codePostal, :login, :motPasse, :email)"); 
							$reqEcrire->execute(array('nom' => $_POST['Nom'], 'prenom' => $_POST['Prenom'], 'adresse' => $_POST['Adresse'], 'ville' => $_POST['Ville'], 'province' => $_POST['Province'], 'codePostal' => $_POST['CodePostal'], 'login' => $_POST['login'], 'motPasse' => $mdp, 'email' => $_POST['Courriel']));
							$reqEcrire->closeCursor();

							//Connexion
							$_SESSION['login'] = $_POST['login'];

							header('Location:confirmation.php?confirmer=inscrit');
							exit;
						}
					}
					

				}
			}
			$connexionBD = null;
		}
		?>

		<!-- Contenu -->
		<section id="contenu">
		<h2>inscription</h2><div class="souligne"></div>
		<div id="messageErreur"></div>
		<div id="inscription">
		<form action="inscription.php" method="post" id="formInscription">
			<p>
				<label for="Nom">Nom: </label>
					<input name="Nom" type="text" id="Nom"
				<?php

				if(isset($_POST['Nom']))
				{
					 echo 'value="'.$_POST['Nom'].'"';
				}
				elseif($modification)
				{
					 echo 'value="'.$infoClientEnligne['nom'].'"';
				}
				?>
				/>
				<?php
				if($erreurNom)
				{
					echo "Vous devez entrer un nom valide.";
				}
				?>
			</p>
			<p>
				<label for="Prenom">Prénom: </label>
					<input name="Prenom" type="text" id="Prenom"
					<?php
						if(isset($_POST['Prenom']))
						{
							 echo 'value="'.$_POST['Prenom'].'"';
						}
						elseif($modification)
						{
							 echo 'value="'.$infoClientEnligne['prenom'].'"';
						}
					?>
					/>
				<?php
				if($erreurPrenom)
				{
					echo "Vous devez entrer un prénom valide.";
				}
				?>
			</p>
			<p>
				<label for="Adresse">Adresse: </label>
				<input name="Adresse" type="text" id="Adresse"					
				<?php
						if(isset($_POST['Adresse']))
						{
							 echo 'value="'.$_POST['Adresse'].'"';
						}
						elseif($modification)
						{
							 echo 'value="'.$infoClientEnligne['adresse'].'"';
						}
					?>
					/>
			</p>
			<p>
				<label for="Ville">Ville: </label>
				<input name="Ville" type="text" id="Ville"
				<?php
						if(isset($_POST['Ville']))
						{
							 echo 'value="'.$_POST['Ville'].'"';
						}
						elseif($modification)
						{
							 echo 'value="'.$infoClientEnligne['ville'].'"';
						}
					?>
					/>
				<?php
				if($erreurVille)
				{
					echo "Vous devez entrer une ville valide.";
				}
				?>
			</p>
			<p>
				<label for="Province">Province: </label><br />
				<select name="Province" id="Province">
					<option value = "Alberta">Alberta</option>
					<option value = "CB">Colombie Britanique</option>
					<option value = "IPE">Île-du-Prince-Edouard</option>
					<option value = "Manitoba">Manitoba</option>
					<option value = "NB">Nouveau Brunswick</option>
					<option value = "NE">Nouvelle-Écosse</option>
					<option value = "Ontario">Ontario</option>
					<option value = "Québec" selected>Québec</option>
					<option value = "Saskatchewan">Saskatchewan</option>
					<option value = "TNL">Terre-Neuve et Labrador</option>
				</select>
			</p>
			<p>
				<label for="CodePostal">Code Postal: </label>
				<input name="CodePostal" type="text" id="CodePostal"
				<?php
						if(isset($_POST['CodePostal']))
						{
							 echo 'value="'.$_POST['CodePostal'].'"';
						}
						elseif($modification)
						{
							 echo 'value="'.$infoClientEnligne['codePostal'].'"';
						}
					?>
					/>
				<?php
				if($erreurCodePostal)
				{
					echo "Vous devez entrer le code postal comme ceci : A1A 1A1";
				}
				?>				
			</p>
			<?php
		
			if(!($modification))
			{
				?>
				<p>
				<label for="login">login: </label>
				<input type="text" name="login" id="login" class="vertpale"
				<?php
						if(isset($_POST['login']))
						{
							 echo 'value="'.$_POST['login'].'"';
						}
					?>
					/>
				<?php
				if($erreurLoginExiste)
				{
					echo "Ce login existe déjà.";
				}
				if($erreurLogin)
				{
					echo "Vous devez entrer un login valide.";
				}
				?>
				</p>
				<p id="erreurLogin"></p>
				<?php
			}
			else
			{?>
					<p>
				<label for="login">login: </label>
				<input type="text" name="login" id="login" value="<?php echo $infoClientEnligne['login'];?>" readonly>
				</p>
			<?php
			
			}
				
				?>				
			<p>
				<label for="Courriel">Adresse Courriel: </label>
				<input type="text" name="Courriel" id="Courriel"
				<?php
						if(isset($_POST['Courriel']))
						{
							 echo 'value="'.$_POST['Courriel'].'"';
						}
						elseif($modification)
						{
							 echo 'value="'.$infoClientEnligne['email'].'"';
						}
					?>
					/>
				<?php
				if($erreurCourrielExiste)
				{
					echo "Ce courriel existe déjà.";
				}
				if($erreurCourriel)
				{
					echo "Vous devez entrer un courriel valide.";
				}
				?>
			</p>
			<?php
			if(!($modification))
			{
				?>
				<p>
					<label for="passe">mot de passe: </label>
					<input type="password" name="passe" id="passe" />
					<?php
					if($erreurMotPasse)
					{
						echo "Vous devez entrer un mot de passe valide.";
					}
					?>
				</p>
			<?php
			}
			?>
			<p class="boutons">
				<input name="envoyer" type="submit" id="envoyer" class="btn"
				<?php
					if($modification)
					{
						echo 'value="Modifier"';
					}
					else
					{
						echo 'value="Inscription"';
					}
				?>
				/>
			</p>
			</form>
			</div>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
		  <script type="text/javascript" src="js/validation.js"></script>
		  <script src="js/util-dom.js"></script>
</body>
</html>