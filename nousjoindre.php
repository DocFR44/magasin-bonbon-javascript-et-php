<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>m.gummy | nous joindre</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
		<link href="https://fonts.googleapis.com/css?family=Rubik:400" rel="stylesheet">
    </head>
<body>
	<!-- Division principale qui contient tous les éléments de la page -->
	<div id="page">
		
		<!-- En-tête de la page -->
		<?php
			include 'includes/header.php';
		?>
		<!-- Menu principal -->
					<?php
			include 'includes/menu.php';
		?>
		<!-- Contenue -->
		<section id="contenu">
		<img src="images/imagenousjoindre.jpg" alt="Magnifique image de bonbon dur" />
		<h2>pour nous joindre </h2><div class="souligne"></div>

		<div id ="aproposbloc1">
			<h3>adresse</h3>
			<p>2700, Boul. Laurier</p>
			<p>Québec (Qc)  G1V 2L8</p>
			<p>418 651-5000</p>
		</div>
		<div id ="aproposbloc2">
			<h3>heures d'ouverture</h3>
			<p>Lundi: 10h00 - 18h00</p>
			<p>Mardi: 10h00 - 18h00</p>
			<p>Mercredi: 10h00 - 18h00</p>
			<p>Jeudi: 10h00 - 21h00</p>
			<p>Vendredi: 10h00 - 21h00</p>
			<p>Samedi: 9h00 - 17h00</p>
			<p>Dimanche: 10h00 - 17h00</p>
		</div>
		<div id ="aproposbloc3">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2732.7112192229297!2d-71.28725018439833!3d46.77058837913804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cb896cd3ffacff9%3A0xf14528ad14d1e9b7!2sLaurier+Qu%C3%A9bec%2C+2700+Boulevard+Laurier%2C+Ville+de+Qu%C3%A9bec%2C+QC+G1V+2L8!5e0!3m2!1sfr!2sca!4v1475275055276" width="850" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		</section><!-- Fin de la section "contenu" -->
		<!-- Pied de page -->
				<?php
			include 'includes/footer.php';
		?>

	</div> <!-- Fin de la division "page" -->
</body>
</html>
