		<?php
			include 'includes/session.php';
			include 'includes/param_bd.inc';
			$siItemEnelver = false;
			if(!isset($_GET['enleverbonbon']))
			{				
				if (($_SESSION['panier'][$_GET['numproduit']] > 1))
				{
					$_SESSION['panier'][$_GET['numproduit']] -=1;
				}
				else
				{
					unset($_SESSION['panier'][$_GET['numproduit']]);
					$siItemEnelver = true;
				}
			}
			
			
			try
				{
					// On se connecte à MySQL
				$connexionBD = new PDO("mysql:host=$dbHote; dbname=$dbNom", $dbUtilisateur, $dbMotPasse, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				// Pour lancer les exceptions lorsqu'il y des erreurs PDO.
				$connexionBD -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}
				
				try
				{
				$req = $connexionBD->prepare('SELECT * FROM produits WHERE produits.no = :item');
				$req->execute(array('item'=>$_GET['numproduit']));

				$infoItem = $req->fetch();

				$req->closeCursor();
				$connexionBD = null;
				}
				catch(Exception $e)
				{
					// En cas d'erreur, on affiche un message et on arrête tout
				        die('Erreur : '.$e->getMessage());
				}

					if($siItemEnelver)
					{
						$quantite = 0;
					}
					else
					{
						$quantite = $_SESSION['panier'][$_GET['numproduit']];
					}
					$no = $_GET['numproduit'];
					$prix = $infoItem['prix'];
					$infoItem = "[\n";
					$infoItem .= "\t{\n";
					$infoItem .= "\t\t\"no\": \"$no\",\n";
					$infoItem .= "\t\t\"prix\": \"$prix\",\n";
					$infoItem .= "\t\t\"quantite\": \"$quantite\"\n";
					$infoItem .= "\t},\n";
					$infoItem .= "]";

				$posDerniereVirgule = strrpos($infoItem,",");
				
				$infoItem = substr_replace($infoItem,"",$posDerniereVirgule,1);

				echo $infoItem;
				
				if(isset($_GET['enleverbonbon']))
				{
					unset($_SESSION['panier'][$_GET['numproduit']]);
				}
		?>